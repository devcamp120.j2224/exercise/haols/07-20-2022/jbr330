package com.devcamp.s10.jbr330.restapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s10.jbr330.restapi.model.Employee;

import org.springframework.web.bind.annotation.GetMapping;

@RestController
public class EmployeeController {
    @CrossOrigin
    @GetMapping("/employees")
    public ArrayList<Employee> getLEmployees() {
        ArrayList<Employee> lEmployees = new ArrayList<Employee>();
        Employee employee1 = new Employee(03, "Khoa", "Hoc", 500);
        Employee employee2 = new Employee(15, "Loc", "Ho", 700);
        Employee employee3 = new Employee(94, "Hoa", "Hau", 1000);
        lEmployees.add(employee1);
        lEmployees.add(employee2);
        lEmployees.add(employee3);
        return lEmployees;
    }

}
