package com.devcamp.s10.jbr330.restapi.model;

public class Employee {
    private int id;
    private String firstName;
    private String lastName;
    private int salary;

    public Employee(int id, String firstName, String lastName, int salary) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getSalary() {
        return salary;
    }

    public String getName() {
        return firstName + " " + lastName;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getAnnualSalary() {
        return salary * 12;
    }

    public int raiseSalary(int percent) {
        float newSalary = salary * (1 + ((float) percent / 100));
        return (int) newSalary;
    }

    @Override
    public String toString() {
        return "Employee [firstName=" + firstName + ", id=" + id + ", lastName=" + lastName + ", salary=" + salary
                + "]";
    }

    // public static void main(String[] args) {
    // Employee employee = new Employee(15, "Le", "Hao", 1000);
    // Employee employee1 = new Employee(03, "Khoa", "Hoc", 500);
    // Employee employee2 = new Employee(15, "Loc", "Ho", 700);
    // Employee employee3 = new Employee(94, "Hoa", "Hau", 1000);
    // System.out.println(employee.toString());
    // System.out.println(employee1.toString());
    // System.out.println(employee2.toString());
    // System.out.println(employee3.toString());
    // }
}
